#!/bin/sh
#
# Setup:
# - Create databases for deploy_test and deploy_test_dest
# - Create drush aliases for deploy-test.local and deploy-test-dest.local
# - Add /etc/hosts entries for deploy-test.localhost and deploy-test-dest.localhost
# - Create vhost entries for deploy-test.localhost and deploy-test-dest.localhost
if [ -n "$1" ]; then
    MAKE_PATH="$1"
else
    MAKE_PATH="~/Projects/drupal/deploy_test/build-deploy_test.make"
fi

echo "Rebuild the application source";
sudo rm -rf ~/Sites/deploy_test-build;
sudo rm -rf ~/Sites/deploy_test_dest-build;
drush make $MAKE_PATH ~/Sites/deploy_test-build --working-copy --no-gitinfofile;

cd ~/Sites/deploy_test-build/profiles/deploy_test;
git remote add upstream jrbeeman@git.drupal.org:sandbox/jrbeeman/2114455.git;
git fetch upstream;
git merge upstream/7.x-1.x;

cp -R ~/Sites/deploy_test-build ~/Sites/deploy_test_dest-build;


echo "Reinstall the source site";
drush @loc.deploy-test si --db-url="mysql://drupaluser:@127.0.0.1:33067/deploy_test" --site-name="Deploy test" deploy_test -yv;
chmod -R gou+rwx ~/Sites/deploy_test-build/sites/default/files;
drush @loc.deploy-test en deploy_test_plan -yv;
drush @loc.deploy-test vset -y environment_indicator_overwrite 1;
drush @loc.deploy-test vset -y environment_indicator_overwritten_name "Staging";
drush @loc.deploy-test vset -y environment_indicator_overwritten_color "#339933";


echo "Reinstall the destination site";
drush @loc.deploy-test-dest si --db-url="mysql://drupaluser:@127.0.0.1:33067/deploy_test_dest" --site-name="Deploy test destination" deploy_test -yv;
chmod -R gou+rwx ~/Sites/deploy_test_dest-build/sites/default/files;
drush @loc.deploy-test-dest en deploy_test_endpoint -y;
drush @loc.deploy-test-dest user-create deploytest --mail="deploytest@example.com" --password="deploytest";
drush @loc.deploy-test-dest user-add-role administrator deploytest;
drush @loc.deploy-test-dest vset -y environment_indicator_overwrite 1;
drush @loc.deploy-test-dest vset -y environment_indicator_overwritten_name "Production";
drush @loc.deploy-test-dest vset -y environment_indicator_overwritten_color "#993333";


echo "Done building. Logging in..."

drush @deploy-test.local cc all;
drush @deploy-test.local uli;

drush @deploy-test-dest.local cc all;
drush @deploy-test-dest.local uli;

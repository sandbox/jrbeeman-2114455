api = 2
core = 7.x

includes[] = "drupal-org-core.make"

projects[deploy_test][type] = profile
projects[deploy_test][download][type] = git
projects[deploy_test][download][url] = http://git.drupal.org/sandbox/jrbeeman/2114455.git
projects[deploy_test][download][branch] = 7.x-1.x

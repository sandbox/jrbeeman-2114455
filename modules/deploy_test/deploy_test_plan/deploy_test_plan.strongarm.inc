<?php
/**
 * @file
 * deploy_test_plan.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function deploy_test_plan_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'deploy_auto_plan_name';
  $strongarm->value = '';
  $export['deploy_auto_plan_name'] = $strongarm;

  return $export;
}

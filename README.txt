Sandbox installation profile for testing various deployment-related scenarios.

# Installation

This project makes no attempt at abstracting the install process at the moment.
See deploy_test-build.sh for a sample build process. Current assumptions:

- Source has a Drush alias of @deploy-test.local with a URL of
  http://deploy-test.localhost/
- Destination has a Drush alias of @deploy-test-dest.local with a URL of
  http://deploy-test-dest.localhost/
- Deployment happens via DrupalQueue. Run the following to deploy nodes in the
  plan:
  `drush @deploy-test.local queue-run deploy_deploy -vd`

After building:

1. Create some content on the source site.
2. Navigate to Structure, Deployment. The content you created should be in the
   deployment plan.
3. Run the Drush command above to invoke the deployment.


# TODO

- [ ] Add fieldable panels panes
- [ ] Add https://www.drupal.org/project/workbench_scheduler
- [ ] Add a "last tested date" field to the test plans section below
- [ ] Add Organic Groups
- [ ] Add panels_hash_cache
- [ ] Add callbacks for clearing panels hash cache upon bean change.
- [ ] When a node is saved, clear the panels hash cache.
- [ ] Add hook implementation from https://drupal.org/node/1402860#comment-7798913
- [ ] Add hook implementation from https://drupal.org/comment/8206937#comment-8206937
- [ ] Build a deployment-focused view using VBO.
- [ ] Add a hook to clear derived image styles when a media asset changes.



# Test plans


## Article deployment

1. STAGE: Create an article with all fields, including references and media.
2. STAGE: Navigate to the deployment plan and deploy it.
3. LIVE: Validate article contents on destination site.

Status: Success; Needs re-test.


## Article deployment (with WBM)

1. STAGE: Create an article with all fields filled out. Leave it in draft mode.
2. STAGE: Deploy the article and dependencies.
3. LIVE: Validate article contents and WBM state.
4. STAGE: Change state to published. Redeploy.
5. LIVE: Validate article is now published.

Status: Success; Needs re-test.


## Article hero image update

1. STAGE: Create an article containing a hero image. Deploy.
2. LIVE: Validate the deployment.
3. STAGE: Update the hero image by uploading a new asset. Deploy.
4. LIVE: Validate the deployment.
5. STAGE: Update the hero image by replacing the current image. Deploy.
6. LIVE: Validate the deployment.

Status: Success; Needs re-test.


## Article author update

1. STAGE: Create an article referencing an author (person). Deploy.
2. LIVE: Validate the deployment.
3. STAGE: Edit the author node to change the name. Deploy.
4. LIVE: Validate the deployment by viewing the article node.

Status: Success; Needs re-test.


## Media gallery (field collections)

1. STAGE: Create a media gallery with several assets. Deploy.
2. LIVE: Validate the deployment.
3. STAGE: Edit the gallery and alter assets. Deploy.
4. LIVE: Validate the deployment.

Status: Success; Needs re-test.


## Panelized page deployment (with WBM)

1. STAGE: Create any number of bean blocks.
2. STAGE: Create a Page node in draft mode.
3. STAGE: Deploy the page and dependencies.
4. LIVE: Validate page contents and WBM state.
5. STAGE: Change state of page to published. Redeploy.
6. LIVE: Validate WBM state.
7. On staging, create a new revision of the page and set state to published. Redeploy.
8. On destination, validate contents and WBM state.

Status: Success; Needs re-test.


## Bean update deployment

1. STAGE: Create and deploy a panelized page that contains a bean block.
2. LIVE: Validate the deployment.
3. STAGE: Edit the bean block contents. Redeploy the bean block.
4. LIVE: Validate the change deployed successfully.

Status: Success; Needs re-test.


## Webform

1. STAGE: Create a webform node with several fields attached. Deploy.
2. LIVE: Validate the deployment.

Status: Success; Needs re-test.


## Poll

1. STAGE: Create a poll node with several options. Deploy.
2. LIVE: Validate the deployment.

Status: Needs to be tested.


## Translated node

1. STAGE: Create an article node.
2. STAGE: Create translations of the article.
3. STAGE: Deploy.
2. LIVE: Validate the deployment and translation relationship between deployed nodes.

Status: Success; Needs re-test.


## Menu items

TODO: Define this test.


## Paths

1. STAGE: Create an Article with a custom URL path. Deploy.
2. LIVE: Validate URL path.

Status: Success; Needs re-test.


## Metatag

1. STAGE: Create an Article and provide custom metatag data. Deploy.
2. LIVE: Validate metadata deployed as expected.

Status: Success; Needs re-test.


## Scheduled publishing

Scheduler module.

TODO: Define this test
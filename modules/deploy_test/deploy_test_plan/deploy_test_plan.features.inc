<?php
/**
 * @file
 * deploy_test_plan.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function deploy_test_plan_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "deploy" && $api == "deploy_endpoints") {
    return array("version" => "1");
  }
  if ($module == "deploy" && $api == "deploy_plans") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

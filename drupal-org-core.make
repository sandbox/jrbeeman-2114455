api = 2
core = 7.x

; Download Drupal core and apply core patches if needed.
projects[drupal][type] = core
projects[drupal][version] = 7.28
projects[drupal][download][type] = get
projects[drupal][download][url] = http://ftp.drupal.org/files/projects/drupal-7.28.tar.gz
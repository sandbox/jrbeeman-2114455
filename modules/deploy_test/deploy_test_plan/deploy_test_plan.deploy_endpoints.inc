<?php
/**
 * @file
 * deploy_test_plan.deploy_endpoints.inc
 */

/**
 * Implements hook_deploy_endpoints_default().
 */
function deploy_test_plan_deploy_endpoints_default() {
  $export = array();

  $endpoint = new DeployEndpoint();
  $endpoint->disabled = FALSE; /* Edit this to true to make a default endpoint disabled initially */
  $endpoint->api_version = 1;
  $endpoint->name = 'deploy_test_endpoint';
  $endpoint->title = 'deploy_test_endpoint';
  $endpoint->description = '';
  $endpoint->debug = 0;
  $endpoint->authenticator_plugin = 'DeployAuthenticatorSession';
  $endpoint->authenticator_config = array(
    'username' => 'deploytest',
    'password' => 'deploytest',
  );
  $endpoint->service_plugin = 'DeployServiceRestJSON';
  $endpoint->service_config = array(
    'url' => 'http://deploy-test-dest.localhost/dt-services',
  );
  $export['deploy_test_endpoint'] = $endpoint;

  return $export;
}

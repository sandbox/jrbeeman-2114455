<?php
/**
 * @file
 * deploy_test_ct_media_gallery.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function deploy_test_ct_media_gallery_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-media_gallery-field_gallery_items'
  $field_instances['node-media_gallery-field_gallery_items'] = array(
    'bundle' => 'media_gallery',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'field_collection',
        'settings' => array(
          'add' => 'Add',
          'delete' => 'Delete',
          'description' => TRUE,
          'edit' => 'Edit',
          'view_mode' => 'full',
        ),
        'type' => 'field_collection_view',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_gallery_items',
    'label' => 'Gallery items',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'field_collection',
      'settings' => array(),
      'type' => 'field_collection_embed',
      'weight' => 1,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Gallery items');

  return $field_instances;
}

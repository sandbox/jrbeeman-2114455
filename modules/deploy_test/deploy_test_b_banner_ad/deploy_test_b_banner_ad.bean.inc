<?php
/**
 * @file
 * deploy_test_b_banner_ad.bean.inc
 */

/**
 * Implements hook_bean_admin_ui_types().
 */
function deploy_test_b_banner_ad_bean_admin_ui_types() {
  $export = array();

  $bean_type = new stdClass();
  $bean_type->disabled = FALSE; /* Edit this to true to make a default bean_type disabled initially */
  $bean_type->api_version = 5;
  $bean_type->name = 'banner_ad';
  $bean_type->label = 'Banner ad';
  $bean_type->options = '';
  $bean_type->description = '';
  $export['banner_ad'] = $bean_type;

  return $export;
}

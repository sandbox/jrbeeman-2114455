<?php
/**
 * @file
 * deploy_test_plan.deploy_plans.inc
 */

/**
 * Implements hook_deploy_plans_default().
 */
function deploy_test_plan_deploy_plans_default() {
  $export = array();

  $plan = new DeployPlan();
  $plan->disabled = FALSE; /* Edit this to true to make a default plan disabled initially */
  $plan->api_version = 1;
  $plan->name = 'deploy_test_plan';
  $plan->title = 'deploy_test_plan';
  $plan->description = '';
  $plan->debug = 1;
  $plan->aggregator_plugin = 'DeployAggregatorManaged';
  $plan->aggregator_config = array(
    'delete_post_deploy' => 1,
  );
  $plan->fetch_only = 0;
  $plan->processor_plugin = 'DeployProcessorQueue';
  $plan->processor_config = array();
  $plan->endpoints = array(
    'deploy_test_endpoint' => 'deploy_test_endpoint',
  );
  $plan->dependency_plugin = '';
  $export['deploy_test_plan'] = $plan;

  return $export;
}

api = 2
core = 7.x

; General contrib modules
projects[bean][type] = module
projects[bean][subdir] = contrib
projects[bean][version] = 1.x-dev

projects[ctools][type] = module
projects[ctools][subdir] = contrib
projects[ctools][version] = 1.4

projects[date][type] = module
projects[date][subdir] = contrib
projects[date][version] = 2.7

projects[deploy][type] = module
projects[deploy][subdir] = contrib
projects[deploy][version] = 2.x-dev

projects[diff][type] = module
projects[diff][subdir] = contrib
projects[diff][version] = 3.2

projects[entity][type] = module
projects[entity][subdir] = contrib
projects[entity][version] = 1.5

projects[entitycache][type] = module
projects[entitycache][subdir] = contrib
projects[entitycache][version] = 1.x-dev
; https://drupal.org/node/1349566
projects[entitycache][patch][] = "https://drupal.org/files/issues/add-translation-information-on-each-request-1349566-23.patch"

projects[entityreference][type] = module
projects[entityreference][subdir] = contrib
projects[entityreference][version] = 1.x-dev

projects[entity_dependency][type] = module
projects[entity_dependency][subdir] = contrib
projects[entity_dependency][version] = 1.x-dev
; https://drupal.org/node/1538848 - deals with malformed or missing entities
projects[entity_dependency][patch][i1538848][url] = "http://drupal.org/files/1538848-missing-bundle-property-5.patch"
projects[entity_dependency][patch][i1538848][md5] = "4531dbecec16e66ca8e1a73147c3fd60"

projects[entity_menu_links][type] = module
projects[entity_menu_links][subdir] = contrib
projects[entity_menu_links][download][type] = git
projects[entity_menu_links][download][url] = "http://git.drupal.org/project/entity_menu_links.git"
projects[entity_menu_links][download][revision] = "f712798"

projects[environment_indicator][type] = module
projects[environment_indicator][subdir] = contrib
projects[environment_indicator][version] = 2.4

projects[features][type] = module
projects[features][subdir] = contrib
projects[features][version] = 2.0

projects[field_collection][type] = module
projects[field_collection][subdir] = contrib
projects[field_collection][download][type] = git
projects[field_collection][download][url] = "http://git.drupal.org/project/field_collection.git"
projects[field_collection][download][revision] = "0fd332e"
; http://drupal.org/node/1316162
projects[field_collection][patch][i1316162][url] = "http://drupal.org/files/field_collection-1316162-192.patch"
projects[field_collection][patch][i1316162][md5] = "e58b42fb92a094525edbb83da195ecbc"
; http://drupal.org/node/1807460
projects[field_collection][patch][i1807460][url] = "http://drupal.org/files/field_collection_with_workbench_moderation-1807460-46.patch"
projects[field_collection][patch][i1807460][md5] = "8c28c0db343ca3e78a2e9b2db0d91966"
; https://drupal.org/node/2000690
projects[field_collection][patch][i2000690][url] = "http://drupal.org/files/2000690-field_collection-node-revision-deletion-issues-3.patch"
projects[field_collection][patch][i2000690][md5] = "e43af98849424be98be0c1226c2e8979"
; https://drupal.org/node/2075325
projects[field_collection][patch][i2075325][url] = "http://drupal.org/files/field_collection_uuid.patch"
projects[field_collection][patch][i2075325][md5] = "6ed84ea8bff79d6da6c9ec322fa03c20"

projects[field_group][type] = module
projects[field_group][subdir] = contrib
projects[field_group][version] = 1.4

projects[file_entity][type] = module
projects[file_entity][subdir] = contrib
projects[file_entity][version] = 2.0-alpha3

projects[flag][type] = module
projects[flag][subdir] = contrib
projects[flag][version] = 2.2

projects[i18n][type] = module
projects[i18n][subdir] = contrib
projects[i18n][version] = 1.11

projects[jquery_update][type] = module
projects[jquery_update][subdir] = contrib
projects[jquery_update][version] = 2.4

projects[libraries][type] = module
projects[libraries][subdir] = contrib
projects[libraries][version] = 2.2

projects[link][type] = module
projects[link][subdir] = contrib
projects[link][version] = 1.2

projects[markdown][type] = module
projects[markdown][subdir] = contrib
projects[markdown][version] = 1.2

projects[media][type] = module
projects[media][subdir] = contrib
projects[media][version] = 2.0-alpha3

projects[metatag][type] = module
projects[metatag][subdir] = contrib
projects[metatag][version] = 1.0-beta9

projects[nodequeue][type] = module
projects[nodequeue][subdir] = contrib
projects[nodequeue][version] = 2.x-dev

projects[panelizer][type] = module
projects[panelizer][subdir] = contrib
projects[panelizer][version] = 3.1
; https://drupal.org/node/2053721
projects[panelizer][patch][] = "https://drupal.org/files/issues/panelizer-n2053721-15.patch"
; https://drupal.org/node/2181799
projects[panelizer][patch][] = "https://www.drupal.org/files/issues/save-multiple-panelizer-displays-2181799.patch"

projects[panelizer_deploy][type] = module
projects[panelizer_deploy][subdir] = contrib
projects[panelizer_deploy][version] = 1.0
; https://drupal.org/node/2210651
projects[panelizer_deploy][patch][] = "https://drupal.org/files/issues/panelizer_deploy-bean_dependency-2210651-1.patch"

projects[panels][type] = module
projects[panels][subdir] = contrib
projects[panels][version] = 3.x-dev

projects[redirect][type] = module
projects[redirect][subdir] = contrib
projects[redirect][download][type] = git
projects[redirect][download][url] = "http://git.drupal.org/project/redirect.git"
projects[redirect][download][revision] = "0b7b8dc"
; https://drupal.org/node/1517348#comment-6517838
projects[redirect][patch][i1517348][url] = "http://drupal.org/files/redirect-1517348-uuid-9.patch"
projects[redirect][patch][i1517348][md5] = "2f0b8ba8e56cfda2fcd4a88e3f1bb0ba"

projects[services][type] = module
projects[services][subdir] = contrib
projects[services][version] = 3.7

projects[services_basic_auth][type] = module
projects[services_basic_auth][subdir] = contrib
projects[services_basic_auth][version] = "1.1"

projects[stage_file_proxy][type] = module
projects[stage_file_proxy][subdir] = contrib
projects[stage_file_proxy][version] = "1.4"

projects[strongarm][type] = module
projects[strongarm][subdir] = contrib
projects[strongarm][version] = 2.0

projects[token][type] = module
projects[token][subdir] = contrib
projects[token][version] = 1.5

projects[uuid][type] = module
projects[uuid][subdir] = contrib
projects[uuid][download][type] = git
projects[uuid][download][url] = "http://git.drupal.org/project/uuid.git"
projects[uuid][download][revision] = "3f4d9fb5e45a147980a89e611f8c124be5c69948"
; http://drupal.org/node/2074599 - remove contrib support from the uuid.core.inc so that it can be included separately
projects[uuid][patch][] = "http://drupal.org/files/uuid_remove_contrib_support-2074599-2.patch"
; https://drupal.org/node/2074621 - field collection - patch uuid services to
; not check access callback for field collections need a better way to do this
; but for now:
projects[uuid][patch][] = "http://drupal.org/files/uuid_services_field_collection_revisions.patch"
; https://drupal.org/node/1795152
projects[uuid][patch][] = "https://drupal.org/files/localise-tnid-post-save-1795152-1.patch"

projects[variable][type] = module
projects[variable][subdir] = contrib
projects[variable][version] = 2.5

projects[views][type] = module
projects[views][subdir] = contrib
projects[views][version] = 3.8

projects[views_bulk_operations][type] = module
projects[views_bulk_operations][subdir] = contrib
projects[views_bulk_operations][version] = 3.2

projects[webform][type] = module
projects[webform][subdir] = contrib
projects[webform][version] = 3.20
; http://drupal.org/node/2076483
; Careful: subsequent patches in that issue don't work as expected.
projects[webform][patch][] = "https://drupal.org/files/webform_uuid_0.patch"

projects[workbench_moderation][type] = module
projects[workbench_moderation][subdir] = contrib
projects[workbench_moderation][version] = 1.x-dev
; https://drupal.org/node/2096225
projects[workbench_moderation][patch][] = "https://drupal.org/files/workbench-move_hook_features_api-2096225-1.patch"
; https://drupal.org/node/1330562
projects[workbench_moderation][patch][] = "https://drupal.org/files/issues/1330562-workbench-moderation-moderate-cache-clear-26.patch"
; https://drupal.org/node/1402860
projects[workbench_moderation][patch][] = "https://drupal.org/files/workbench_moderation-panelizer_revisions-1402860-36.patch"
; https://drupal.org/node/1787214
projects[workbench_moderation][patch][] = "https://drupal.org/files/workbench_moderation-vbo-1787214-12.patch"
; https://drupal.org/node/1919706
projects[workbench_moderation][patch][] = "https://drupal.org/files/issues/workbench_moderation-entitymalformed-1919706-3.patch"

projects[workbench_scheduler][type] = module
projects[workbench_scheduler][subdir] = contrib
projects[workbench_scheduler][version] = 1.3
; https://www.drupal.org/node/2266691
projects[workbench_scheduler][patches][] = "https://www.drupal.org/files/issues/workbench_scheduler.optional-states-4.2266691.patch"
; https://www.drupal.org/node/2282649
projects[workbench_scheduler][patches][] = "https://www.drupal.org/files/issues/workbench_scheduler.views_left_outer_join.2282649.patch"

projects[xautoload][type] = module
projects[xautoload][subdir] = contrib
projects[xautoload][version] = 4.5


; Development
projects[coder][type] = module
projects[coder][subdir] = development
projects[coder][version] = 2.2

projects[devel][type] = module
projects[devel][subdir] = development
projects[devel][version] = 1.5

projects[XHProf][type] = module
projects[XHProf][subdir] = development
projects[XHProf][version] = 1.0-beta2


; Themes
projects[shiny][type] = theme
projects[shiny][subdir] = contrib
projects[shiny][version] = 1.5

projects[bootstrap][type] = theme
projects[bootstrap][subdir] = contrib
projects[bootstrap][version] = 3.0


; Libraries
libraryes[spyc][type] = libraries
libraries[spyc][download][type] = get
libraries[spyc][download][url] = "https://raw.github.com/mustangostang/spyc/master/Spyc.php"
libraries[spyc][directory_name] = spyc

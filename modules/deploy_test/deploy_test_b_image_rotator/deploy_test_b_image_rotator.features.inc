<?php
/**
 * @file
 * deploy_test_b_image_rotator.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function deploy_test_b_image_rotator_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "bean_admin_ui" && $api == "bean") {
    return array("version" => "5");
  }
}

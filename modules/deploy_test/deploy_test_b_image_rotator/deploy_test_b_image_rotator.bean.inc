<?php
/**
 * @file
 * deploy_test_b_image_rotator.bean.inc
 */

/**
 * Implements hook_bean_admin_ui_types().
 */
function deploy_test_b_image_rotator_bean_admin_ui_types() {
  $export = array();

  $bean_type = new stdClass();
  $bean_type->disabled = FALSE; /* Edit this to true to make a default bean_type disabled initially */
  $bean_type->api_version = 5;
  $bean_type->name = 'image_rotator';
  $bean_type->label = 'Image rotator';
  $bean_type->options = '';
  $bean_type->description = '';
  $export['image_rotator'] = $bean_type;

  return $export;
}
